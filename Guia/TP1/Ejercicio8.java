package Guia.TP1;

import java.util.Scanner;

public class Ejercicio8 {


    public static void main(String[] args) {
        
        Scanner s = new Scanner(System.in);
        float temp,kelvin,fahren;
        System.out.println("Ingrese una temperatura en grados Celcius: ");
        temp = s.nextFloat();
        kelvin =  (float) (273.15 + temp);
        fahren = (float) ((1.8 * temp)+ 32);
        System.out.println("La temperatura "+temp+"�C");
        System.out.println(kelvin+"K");
        System.out.println(fahren+"�F");
        
    }
    
}
