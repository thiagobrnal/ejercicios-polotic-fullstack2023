package Guia.TP1;

import java.util.Scanner;

public class Ejercicio9 {


    public static void main(String[] args) {
        
        double dolar, euro, guarani, real;
        Scanner s = new Scanner(System.in);
        System.out.println("Ingrese los pesos Argentinos para la conversion: ");
        double peso = s.nextDouble();
        dolar = peso / 231.68;
        euro = peso / 250.69;
        guarani= peso * 31.00;
        real = peso / 46.81;
        System.out.printf("El valor de $"+peso+" en dolares es $%.2f", dolar);
        System.out.printf("%nEl valor de $"+peso+" en euros es $%.2f", euro);
        System.out.printf("%nEl valor de $"+peso+" en guaran�es es $%.2f", guarani);
        System.out.printf("%nEl valor de $"+peso+" en reales es $%.2f %n", real);
    }
    
}
