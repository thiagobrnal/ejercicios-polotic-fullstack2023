package Guia.TP1;

import java.util.Scanner;

public class Ejercicio7 {


    public static void main(String[] args) {
        
        Scanner s = new Scanner(System.in);
        int edad1,edad2,aux;
        System.out.println("Ingrese la primera edad: ");
        edad1 = s.nextInt();
        System.out.println("Ingrese la segunda edad: ");
        edad2 = s.nextInt();
        aux = edad1;
        edad1 = edad2;
        edad2 = aux;
        System.out.println("La primera edad ahora es "+edad1+" y la segunda edad es "+edad2);
    }
    
}
