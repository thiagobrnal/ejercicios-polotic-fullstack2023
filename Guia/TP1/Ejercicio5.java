package Guia.TP1;

import java.util.Scanner;

public class Ejercicio5 {


    public static void main(String[] args) {
        
        Scanner s = new Scanner(System.in);
        double area, radio, perim;
        System.out.println("Ingrese el radio de la circunferencia: ");
        radio =s.nextDouble();
        area = Math.PI*(radio*radio);
        perim = 2*Math.PI*radio;
        System.out.println("El area de la circunferencia de radio "+radio+" es "+area);
        System.out.println("El perimetro de la circunferencia de radio "+radio+" es "+perim);
    }
    
}
