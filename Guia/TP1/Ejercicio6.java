package Guia.TP1;

import java.util.Scanner;

public class Ejercicio6 {


    public static void main(String[] args) {
        
        Scanner s = new Scanner(System.in);

        System.out.println("Ingrese el precio del producto: ");
        float producto = s.nextFloat();
        System.out.println("Ingrese el porcentaje del descuento: ");
        float porcentaje = s.nextFloat();
        float descuento = (producto * porcentaje)/100; 
        float total = producto - descuento;

        // no informa el importe descontado
        System.out.println("Se le aplicó "+porcentaje+"% al total del producto");
        System.out.println("Se descontó $"+descuento+" al total y quedó en $"+total);
        
    }
    
}
