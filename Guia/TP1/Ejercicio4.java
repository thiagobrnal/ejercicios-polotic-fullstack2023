package Guia.TP1;

import java.util.Scanner;

public class Ejercicio4 {


    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int cont = 1;
        float acu = 0,res,estatura;
        while(cont<4){
            System.out.println("Ingrese la altura de la persona "+cont+":");
            estatura =s.nextFloat();
            acu = acu + estatura;
            cont++;
        }
        res = acu/(cont - 1);
        System.out.println("El promedio de altura es: "+res);
    }
    
}
