package Guia.TP3;

public class CajaAhorro extends CuentaBancaria {

    @Override
    public void Retirar() {

        Ingresar();

        double aux = getSaldoActual();

        if (deposito > 0) {

            if (deposito > aux) {

                System.out.println("La cantidad a retirar es mayor a la de la cuenta");
                System.out.println("usted retiro: &"+aux);
                setSaldoActual(0);
                System.out.println("El saldo de la Cuenta Ahorro es $" + getSaldoActual());
                
            } else {

                aux = aux - deposito;
                setSaldoActual(aux);
                System.out.println("El saldo de la Cuenta Ahorro es $" + getSaldoActual());
                
            }

        } else {
            System.out.println("El importe no es valido");
        }
    }

}
