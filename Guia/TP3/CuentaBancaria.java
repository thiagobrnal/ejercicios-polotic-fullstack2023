package Guia.TP3;

import java.util.Scanner;

public abstract class CuentaBancaria {

    Scanner s = new Scanner(System.in);
    protected double deposito;
    //Atributos de la cuenta bancariaS
    private int numero;
    private long dni;
    private double saldoActual;

    public CuentaBancaria() {
    }

    public CuentaBancaria(int numero, long dni, double saldoActual) {

        this.numero = numero;
        this.dni = dni;
        this.saldoActual = saldoActual;
    }

    public void Operaciones() {
        int bandera = 0, seleccion = 0;

        do {
            do {
                System.out.println("Menu de Cuenta");
                System.out.println("1. Ingresar dinero");
                System.out.println("2. Extraccion rapida");
                System.out.println("3. Consultar saldo");
                System.out.println("4. Consultar Datos");
                System.out.println("5. Retirar");
                System.out.println("6. Salir");
                seleccion = s.nextInt();

                if (seleccion >= 1 && seleccion <= 6) {
                    bandera = 1;
                } else {
                    System.out.println("Operacion no valida");
                    System.out.println("---------------------------");
                }
            } while (bandera == 0);

            switch (seleccion) {
                case 1:
                    Ingresar();
                    if (deposito >= 0) {
                        saldoActual += deposito;
                        System.out.println("El ingreso se realizo con exito");
                        System.out.println("---------------------------");
                    } else {
                        System.out.println("El ingreso de dinero no es valido");
                        System.out.println("---------------------------");
                    }
                    break;
                case 2:
                    extraccionRapida();
                    break;
                case 3:
                    consultarSaldo();
                    break;
                case 4:
                    consultarDatos();
                    break;
                case 5:
                    Retirar();
                    break;
                case 6:
                    System.out.println("---------------------------");
                    System.out.println("Gracias por elegirnos");
                    System.out.println("---------------------------");

                    bandera = 2;
                    break;
                default:
                    throw new AssertionError();
            }
        } while (bandera != 2);
    }

    public void Ingresar() {
        System.out.print("Ingrese la cantidad $");
        deposito = s.nextDouble();
    }

    public void extraccionRapida() {

        double aux = 0;
        int porcentaje = 20;

        if (saldoActual <= 0) {
            System.out.println("---------------------------");
            System.out.println("La cuenta no posee dinero");
            System.out.println("---------------------------");
        } else {
            aux = (saldoActual * porcentaje) / 100;
            saldoActual = saldoActual - aux;

            System.out.println("---------------------------");
            System.out.println("El importe que retiro es $" + aux);
            System.out.println("---------------------------");
        }
    }

    public void consultarSaldo() {
        System.out.println("---------------------------");
        System.out.println("Saldo Cuenta:" + saldoActual);
        System.out.println("---------------------------");
    }

    public void consultarDatos() {
        System.out.println("---------------------------");
        System.out.println("Num. Cuenta:" + numero);
        System.out.println("Dni:" + dni);
        System.out.println("---------------------------");

    }

    public abstract void Retirar();

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public long getDni() {
        return dni;
    }

    public void setDni(long dni) {
        this.dni = dni;
    }

    public double getSaldoActual() {
        return saldoActual;
    }

    public void setSaldoActual(double saldoActual) {
        this.saldoActual = saldoActual;
    }
}
