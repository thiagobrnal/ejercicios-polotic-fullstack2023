
package Guia.TP3;

import java.util.Scanner;


public class Libro {
    
    private long isbn;
    private String titulo;
    private String autor;
    private int numPaginas;
    
    public Libro(){
    }
    
    public Libro(long isbn, String titulo, String autor, int numPaginas){
        this.isbn = isbn;
        this.titulo = titulo;
        this.autor = autor;
        this.numPaginas = numPaginas;
    }

    public static void main(String[] args) {
        
        String titulo, autor;
        long isbn;
        int numPaginas;
        
        Scanner s = new Scanner(System.in);
        System.out.println("Ingrese el isbn: ");
        isbn = s.nextLong();
        
        s.nextLine();
        
        
        
        System.out.println("Ingrese el titulo del libro: ");
        titulo = s.nextLine();
        
        System.out.println("Ingrese el autor del libro: ");
        autor = s.nextLine();
        
        System.out.println("Ingrese la cantidad de paginas del libro: ");
        numPaginas = s.nextInt();
        
        
         Libro lb1 = new Libro(isbn, titulo, autor, numPaginas);
         
         System.out.println(lb1);
    }
    
    @Override
    public String toString(){
        return "El libro "+titulo+" isbn "+isbn+" hecho por "+autor+" con "+numPaginas+" paginas";
    }
    
}
