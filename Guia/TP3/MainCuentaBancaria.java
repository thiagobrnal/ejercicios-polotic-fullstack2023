package Guia.TP3;

import java.util.Scanner;

public class MainCuentaBancaria {

    public static void main(String[] args) {

        double saldo = 0;
        long dni = 0;
        int seleccion = 0, num = 0;
        Scanner s = new Scanner(System.in);

        System.out.println("Eliga el tipo de cuenta");
        System.out.println("1. Cuenta Corriente");
        System.out.println("2. Cuenta Ahorro");
        seleccion = s.nextInt();
        
        num = 1;
        System.out.println("Ingrese dni");
        dni = s.nextLong();
        System.out.println("Ingrese saldo inicial");
        saldo = s.nextDouble();
        

        if (seleccion >= 1 && seleccion <= 2) {
            if (seleccion == 1) {
                CuentaBancaria cliente = new CuentaCorriente();
                cliente.setNumero(num);
                cliente.setSaldoActual(saldo);
                cliente.setDni(dni);
                cliente.Operaciones();
            }else if (seleccion == 2) {
                CuentaBancaria cliente = new CajaAhorro();
                cliente.setNumero(num);
                cliente.setSaldoActual(saldo);
                cliente.setDni(dni);
                cliente.Operaciones();
            }
        } else {
            System.out.println("seleccion no valida");
        }

        

    }
}
