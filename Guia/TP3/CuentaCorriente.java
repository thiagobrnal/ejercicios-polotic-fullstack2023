
package Guia.TP3;

public class CuentaCorriente extends CuentaBancaria {

    @Override
    public void Retirar() {
        
        Ingresar();
        
        if (deposito>0) {
            
           double aux = getSaldoActual();
           aux = aux - deposito;
           setSaldoActual(aux);
            System.out.println("El saldo de la Cuenta Corriente es $"+getSaldoActual());
            
        } else {
            System.out.println("El importe no es valido");
        }
    }
    
}
