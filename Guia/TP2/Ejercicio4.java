package Guia.TP2;

import java.util.Scanner;

public class Ejercicio4 {
    
    
    public static void main(String[] args) {
        
       Scanner s = new Scanner(System.in);
       int numero,factorial = 1;
       
        System.out.println("Ingrese un numero entero; ");
        numero = s.nextInt();
        
        for(int i=1;i<=numero;i++){
            factorial =factorial * i;
        }
        
        System.out.println("El factorial de "+numero+"! = "+factorial);
    }  
    
}
