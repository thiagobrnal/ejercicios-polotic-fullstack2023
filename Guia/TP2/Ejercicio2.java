package Guia.TP2;

import java.util.Scanner;

public class Ejercicio2 {


    public static void main(String[] args) {
        
        String palabra = "", invertida = "";
        int cantidad;
        Scanner s = new Scanner(System.in);
        System.out.println("Ingrese una palabra: ");
        palabra = s.nextLine();
        for (cantidad = palabra.length() - 1; cantidad >= 0;cantidad--){
            invertida += palabra.charAt(cantidad);
        }
        if(palabra.equalsIgnoreCase(invertida)){
            System.out.println("Es palíndromo");
        }else{
            System.out.println("No es palíndromo");        
        }
    }  
}
