package Guia.TP2;

public class Ejercicio6{
    public static void main(String[] args) {
        int limite = 200;
        
        System.out.println("Listado de n�meros primos menores que " + limite + ":");
        
        for (int i = 2; i < limite; i++) {
            if (esPrimo(i)) {
                System.out.print(i);
            }
        }
    }
    
    public static boolean esPrimo(int numero) {
        if (numero <= 1) {
            return false;
        }
        
        for (int i = 2; i <= Math.sqrt(numero); i++) {
            if (numero % i == 0) {
                return false;
            }
        }
        
        return true;
    }
}

