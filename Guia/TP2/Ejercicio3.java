package Guia.TP2;

import java.util.Scanner;

public class Ejercicio3 {
    private Scanner s;
    private int[] numeros;
    int tam = 5;
    

    public void Cargar(){
        s = new Scanner(System.in);
        numeros = new int[tam];
        for(int i=0;i<numeros.length;i++){
            System.out.println("Ingrese un numero natural "+(i+1)+": ");
            numeros[i] = s.nextInt();
        }
    }
    
    public void Ordenar(){
        for(int j = 0; j<tam - 1; j++){
            for(int i = 0; i<tam - 1; i++){
                if(numeros[i]>numeros[i+1]){
                    int aux;
                    aux = numeros[i];
                    numeros[i] = numeros[i+1];
                    numeros[i+1] = aux;
                }
            }
        }
    }
    
    public void Mostrar(){
        System.out.println("\nDe forma ascendente el vector ordenado");
        for(int i=0;i<numeros.length;i++){
            System.out.println(numeros[i]);
        }
    }
    
    public static void main(String[] args) {
        Ejercicio3 ej3=new Ejercicio3();
        ej3.Cargar();
        ej3.Ordenar();
        ej3.Mostrar();
    }  
    
}
